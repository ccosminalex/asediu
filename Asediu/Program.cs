﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Asediu
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;
            Console.Write("Introduceti a: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Introduceti b: ");
            b = int.Parse(Console.ReadLine());
            Console.Write("Supravietuitorul este: ");
            Supravietuitorul(a, b);
            Console.ReadKey();
        }
        static void Supravietuitorul(int a, int b)
        {
            int pos = -1;
            int[] v = new int[a];
            for(int i = 0; i < a; i++)
            {
                v[i] = i + 1;
            }
            do
            {
                for (int i = 1; i <= b; i++)
                {
                    if (pos == v.Length - 1)
                        pos = 0;
                    else
                        pos++;
                }
                for (int j = pos; j < v.Length - 1; j++)
                {
                    v[j] = v[j + 1];
                }
                pos--;
                Array.Resize(ref v, v.Length - 1);
            } while (v.Length > 1);
            Console.Write(v[0]);
        }
    }
}
